const axios = require('axios');

async function checkAuth(authorization) {
  try {
    const method = await axios({
      method: 'GET',
      url: 'https://api.securesign.org/account/details',
      headers: {Authorization: authorization}
    });
    if (method.data.code === 1000) return true;
    else return false;
  } catch (err) { return false; }
}

module.exports.checkAuth = checkAuth;