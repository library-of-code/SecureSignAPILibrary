const { EventEmitter } = require('events');
const axios = require('axios');
const SecureSignError = require('./SecureSignError');

class Key extends EventEmitter {
  /**
   * After init, please call `Key.setAuthorization();`.
   * @class
   * @example new Key().setAuthorization('hash');
   */
  constructor() {
    super();
    this.keys = [];
    this.emit('init', this);
  }
  /**
   * This function sets your authorization, you should call this function before any other one.
   * @param {String} authorization Your authorization hash token;
   * @returns {void}
   */
  setAuthorization(authorization) {
    this._authorization = authorization;
  }
  /**
   *
   * Creates a new ECC private key.
   * @param {String} curve The name of the curve to use in ECC key generation.
   * @returns {Promise<this>}
   * @example Key.createECC('prime256v1').then(r => console.log(r)).catch(e => console.error(e)); 
   */
  async createECC(curve) {
    if (!this._authorization) {
      this.emit('error', `Cannot read property ${this._authorization} of 'undefined'`);
      throw new SecureSignError('Call "Key.setAuthorization" to set key authorization before calling "Key.createECC".');
    }
    if (typeof curve !== 'string') {
      this.emit('error', `Expecting 'string' on property curve, received type '${typeof curve}' instead.`);
      throw new TypeError(`Expecting 'string' on property curve, received type '${typeof curve}' instead.`);
    }
    try {
      const method = await axios({
        method: 'post',
        url: 'https://api.securesign.org/keys/ecc',
        headers: {'Authorization': this._authorization, 'Content-Type': 'application/json'},
        data: JSON.stringify({ curve })
      });
      this.emit('generation', method.data);
      this.keys.push({type: 'ECC', curve: curve, key: method.data.message});
      return this;
    } catch (err) {
      this.emit('error', err);
      throw new SecureSignError(err);
    }
  }

  /**
   * 
   * Creates a new RSA private key.
   * @param {Number} mod The modulus for the private key, 2048 is common. 
   * @returns {Promise<this>}
   * @example Key.createRSA(2048).then(r => console.log(r)).catch(e => console.error(e));
   */
  async createRSA(mod) {
    if (!this._authorization) {
      this.emit('error', `Cannot read property ${this._authorization} of 'undefined'`);
      throw new SecureSignError('Call "Key.setAuthorization" to set key authorization before calling "Key.createECC".');
    }
    if (typeof mod !== 'number') {
      this.emit('error', `Expecting 'number' on property mod, received type '${typeof mod}' instead.`);
      throw new TypeError(`Expecting 'number' on property mod, received type '${typeof mod}' instead.`);
    }
    if (mod > 8002 || mod < 1024) throw new RangeError('Modulus is out of range, expected greater than 1024 and lower than 8002.'); 
    try {
      const method = await axios({
        method: 'post',
        url: 'https://api.securesign.org/keys/rsa',
        headers: {'Authorization': this._authorization, 'Content-Type': 'application/json'},
        data: JSON.stringify({modulus: mod})
      });
      this.emit('generation', method.data);
      this.keys.push({type: 'RSA', modulus: mod, key: method.data.message});
      return this;
    } catch (err) {
      this.emit('error', err);
      throw new SecureSignError(err);
    }
  }

  /**
   * 
   * Gets a list of the ECC curves that can be used.
   * @returns {Promise<String>}
   */
  async getECCCurves() {
    if (!this._authorization) {
      this.emit('error', `Cannot read property ${this._authorization} of 'undefined'`);
      throw new SecureSignError('Call "Key.setAuthorization" to set key authorization before calling "Key.createECC".');
    }
    try {
      const method = await axios({
        method: 'get',
        url: 'https://api.securesign.org/keys/ecc',
        headers: {'Authorization': this._authorization},
      });
      return method.data.message;
    } catch (err) {
      this.emit('error', err);
      throw new SecureSignError(err);
    }
  }
}

module.exports = Key;