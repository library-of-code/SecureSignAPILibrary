'use strict';
class SecureSignError extends Error {
  constructor(...args) {
    super(...args);
    this.name = 'SecureSignError';
    Error.captureStackTrace(this, SecureSignError);
  }
}

module.exports = SecureSignError;